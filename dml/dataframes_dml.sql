insert into PICO_DATAFRAMES (pico_id, "timestamp", pico_temp, sens_temp0, sens_temp1, sens_press0,
                             sens_press1, sens_hum0, sens_hum1, sens_accelX0, sens_accelX1, sens_accelY0, sens_accelY1,
                             sens_accelZ0, sens_accelZ1, laser0_state, laser1_state, stepper0_speed, stepper1_speed,
                             message_id)
values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);