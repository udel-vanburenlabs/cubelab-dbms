insert into CASE_PARAMS (flow_facility, capture_duration_sec, stepper_speed, case_type_id, spin_duration_sec,
                         laser_brightness, min_sec_since_last, max_temp, min_temp, capture_type_id, burst_period_ms,
                         burst_size, target_fps)
values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);