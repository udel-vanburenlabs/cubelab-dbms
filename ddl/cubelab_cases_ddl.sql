create schema if not exists CUBELAB_CASES;
set search_path = "cubelab_cases";

create table if not exists CASE_TYPES (
    case_type_id serial primary key,
    case_type varchar unique not null
);

create table if not exists CAPTURE_TYPES (
    capture_type_id serial primary key,
    capture_type varchar unique not null
);

create table if not exists CASE_STATUSES (
    case_status_id serial primary key,
    case_status varchar unique not null
);

-- sets the parameters for each cubelab case
create table if not exists CASE_PARAMS (
    case_id serial primary key,
    flow_facility int not null,
    capture_duration_sec int not null,
    stepper_speed int not null,
    case_type_id int not null, -- steady_state, decay
    spin_duration_sec int not null,
    laser_brightness int not null default 255,
    min_sec_since_last int not null default 0,
    max_temp float,
    min_temp float default 0,
    capture_type_id int not null, -- cont, burst (cont mode will ignore the burst params.)
    burst_period_ms int default 0,
    burst_size int default 2,
    target_fps int,
    backup_priority int not null default 0,

   foreign key (case_type_id) REFERENCES CASE_TYPES(case_type_id),
   foreign key (capture_type_id) REFERENCES CAPTURE_TYPES(capture_type_id),

   check (min_sec_since_last >= 0 and min_temp >= 0 and laser_brightness >= 0 and laser_brightness < 256 and
          stepper_speed >= 0 and stepper_speed <= 1000)
);

create table if not exists CASE_FOOTAGE (
    file_id int not null,
    case_id int not null,
    sequence_id int not null,
    capture_timestamp timestamp not null, -- Use ISO 8601 with microsec precision
    file_path varchar not null,

    primary key (file_id, case_id, sequence_id),
    foreign key (case_id) REFERENCES CASE_PARAMS(case_id)
);

create unique index CASE_FOOTAGE_SEQ_IDX on CASE_FOOTAGE (case_id, sequence_id);

create table if not exists CASE_LOG (
    case_id int not null,
    event_id serial not null,
    timestamp timestamp not null, -- Use ISO 8601 with microsec precision
    log_level int not null default 3,
    log_message varchar default '',

    primary key (case_id, event_id),
    foreign key (case_id) REFERENCES CASE_PARAMS(case_id)
);

create table if not exists CASE_STATUS (
    case_id int primary key,
    case_status_id int not null, -- queued, in_progress, failed, complete
    start_timestamp timestamp, -- Use ISO 8601 with microsec precision
    complete_timestamp timestamp, -- Use ISO 8601 with microsec precision
    average_fps float,
    frames_captured int,

    foreign key (case_id) REFERENCES CASE_PARAMS(case_id),
    foreign key (case_status_id) REFERENCES CASE_STATUSES(case_status_id)
);

insert into CAPTURE_TYPES (capture_type) values ('cont');
insert into CAPTURE_TYPES (capture_type) values ('burst');

insert into CASE_STATUSES (case_status) values ('pending');
insert into CASE_STATUSES (case_status) values ('queued');
insert into CASE_STATUSES (case_status) values ('in_progress');
insert into CASE_STATUSES (case_status) values ('paused');
insert into CASE_STATUSES (case_status) values ('failed');
insert into CASE_STATUSES (case_status) values ('complete');

insert into CASE_TYPES (case_type) values ('steady_state');
insert into CASE_TYPES (case_type) values ('decay');

CREATE OR REPLACE FUNCTION TRG_03_CASE_LOG_PROCEDURE()
    RETURNS TRIGGER
as $TRG_03_CASE_LOG_PROCEDURE$
    begin
        NEW.timestamp := now();
        RETURN NEW;
    end;
$TRG_03_CASE_LOG_PROCEDURE$ language 'plpgsql';

CREATE OR REPLACE FUNCTION TRG_03_CASE_FOOTAGE_PROCEDURE()
    RETURNS TRIGGER
as $TRG_03_CASE_FOOTAGE_PROCEDURE$
    begin
        NEW.capture_timestamp := now();
        RETURN NEW;
    end;
$TRG_03_CASE_FOOTAGE_PROCEDURE$ language 'plpgsql';

-- Create timestamp triggers
CREATE TRIGGER TRG_03_CASE_LOG
BEFORE INSERT ON CASE_LOG
FOR EACH ROW
EXECUTE FUNCTION TRG_03_CASE_LOG_PROCEDURE();

CREATE TRIGGER TRG_04_CASE_LOG
BEFORE UPDATE ON CASE_LOG
FOR EACH ROW
EXECUTE FUNCTION TRG_03_CASE_LOG_PROCEDURE();

CREATE TRIGGER TRG_03_CASE_FOOTAGE
BEFORE INSERT ON CASE_FOOTAGE
FOR EACH ROW
EXECUTE FUNCTION TRG_03_CASE_FOOTAGE_PROCEDURE();

CREATE TRIGGER TRG_04_CASE_FOOTAGE
BEFORE UPDATE ON CASE_FOOTAGE
FOR EACH ROW
EXECUTE FUNCTION TRG_03_CASE_FOOTAGE_PROCEDURE();
