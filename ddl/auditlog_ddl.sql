create schema if not exists AUDITLOG;
set search_path = "auditlog";

create table if not exists CUBELAB_HUB_EVENT_TYPES (
    hub_event_type_id serial primary key,
    hub_event_type varchar unique not null
);

create table if not exists CUBELAB_PICO_EVENT_TYPES (
    pico_event_type_id serial primary key,
    pico_event_type varchar unique not null
);

-- Use this table for logging events on the CubeLab devices and the communication between pico and hub SBC
create table if not exists CUBELAB_AUDIT (
    event_id serial primary key,
    timestamp timestamp not null, -- Use ISO 8601 with microsec precision
    hub_event_type_id int,
    pico_event_type_id int,

    foreign key (hub_event_type_id) REFERENCES CUBELAB_HUB_EVENT_TYPES(hub_event_type_id),
    foreign key (pico_event_type_id) REFERENCES CUBELAB_PICO_EVENT_TYPES(pico_event_type_id)
);

create table if not exists CAMERA_TEMP (
    event_id serial primary key,
    camera_id int not null,
    timestamp timestamp not null,
    temperature float not null,

    unique (event_id, camera_id)
);

CREATE OR REPLACE FUNCTION TRG_03_CUBELAB_AUDIT_PROCEDURE()
    RETURNS TRIGGER
as $TRG_03_CUBELAB_AUDIT_PROCEDURE$
    begin
        NEW.timestamp := now();
        RETURN NEW;
    end;
$TRG_03_CUBELAB_AUDIT_PROCEDURE$ language 'plpgsql';

CREATE TRIGGER TRG_03_CUBELAB_AUDIT
BEFORE INSERT ON CUBELAB_AUDIT
FOR EACH ROW
EXECUTE FUNCTION TRG_03_CUBELAB_AUDIT_PROCEDURE();

CREATE TRIGGER TRG_04_CUBELAB_AUDIT
BEFORE UPDATE ON CUBELAB_AUDIT
FOR EACH ROW
EXECUTE FUNCTION TRG_03_CUBELAB_AUDIT_PROCEDURE();

CREATE TRIGGER TRG_03_CAMERA_TEMP
BEFORE INSERT ON CAMERA_TEMP
FOR EACH ROW
EXECUTE FUNCTION TRG_03_CUBELAB_AUDIT_PROCEDURE();

CREATE TRIGGER TRG_04_CAMERA_TEMP
BEFORE UPDATE ON CAMERA_TEMP
FOR EACH ROW
EXECUTE FUNCTION TRG_03_CUBELAB_AUDIT_PROCEDURE();

insert into CUBELAB_HUB_EVENT_TYPES values (1, 'null');
insert into CUBELAB_PICO_EVENT_TYPES values (1, 'null');
