create schema if not exists DATAFRAMES;
set search_path = "dataframes";

create table if not exists MESSAGES (
    message_id serial primary key,
    message_text varchar unique not null
);

create table if not exists PICO_TIME_OFFSETS (
    pico_id serial primary key,
    pico_start_offset timestamp not null
);

-- Use this table to hold all of the pico telemetry data
create table if not exists PICO_DATAFRAMES (
    frame_id bigserial primary key,
    pico_id int not null,
    timestamp timestamp not null, -- Use ISO 8601 with microsec precision
    pico_temp float,

    sens_temp0 float,
    sens_temp1 float,

    sens_press0 float,
    sens_press1 float,

    sens_hum0 float,
    sens_hum1 float,

    sens_accelX0 float,
    sens_accelX1 float,

    sens_accelY0 float,
    sens_accelY1 float,

    sens_accelZ0 float,
    sens_accelZ1 float,

    sens_gyroX0 float,
    sens_gyroX1 float,

    sens_gyroY0 float,
    sens_gyroY1 float,

    sens_gyroZ0 float,
    sens_gyroZ1 float,

    rpm0 float,
    rpm1 float,

    message_id int not null,

    foreign key (message_id) REFERENCES MESSAGES(message_id)
);

CREATE OR REPLACE FUNCTION TRG_04_PICO_DATAFRAMES_PROCEDURE()
    RETURNS TRIGGER
as $TRG_04_PICO_DATAFRAMES_PROCEDURE$
    begin
    NEW.timestamp := now();
    RETURN NEW;
    end;
$TRG_04_PICO_DATAFRAMES_PROCEDURE$ language 'plpgsql';

CREATE OR REPLACE FUNCTION TRG_04_PICO_TIME_OFFSETS_PROCEDURE()
    RETURNS TRIGGER
as $TRG_04_PICO_TIME_OFFSETS_PROCEDURE$
    begin
        NEW.timestamp := now();
        RETURN NEW;
    end;
$TRG_04_PICO_TIME_OFFSETS_PROCEDURE$ language 'plpgsql';

CREATE OR REPLACE TRIGGER TRG_03_PICO_DATAFRAMES
BEFORE INSERT ON PICO_DATAFRAMES
FOR EACH ROW
EXECUTE FUNCTION TRG_04_PICO_DATAFRAMES_PROCEDURE();

CREATE OR REPLACE TRIGGER TRG_04_PICO_DATAFRAMES
BEFORE UPDATE ON PICO_DATAFRAMES
FOR EACH ROW
EXECUTE FUNCTION TRG_04_PICO_DATAFRAMES_PROCEDURE();

CREATE OR REPLACE TRIGGER TRG_03_PICO_TIME_OFFSETS
BEFORE INSERT ON PICO_TIME_OFFSETS
FOR EACH ROW
EXECUTE FUNCTION TRG_04_PICO_TIME_OFFSETS_PROCEDURE();

CREATE OR REPLACE TRIGGER TRG_04_PICO_TIME_OFFSETS
BEFORE UPDATE ON PICO_TIME_OFFSETS
FOR EACH ROW
EXECUTE FUNCTION TRG_04_PICO_TIME_OFFSETS_PROCEDURE();