# CubeLab DBMS

Various SQL scripts the CubeLab Postgres database.

The database should be configured with PostgreSQL defaults, with the username and password both set to `postgres`.
_This database is not for external use and should never be port forwarded. It should only be visible to `localhost`!_

Please use the scripts in the `/ddl` directory to build the database. The `/dml` directory contains additional scripts
that can be referenced for inserting data, but are not used in DB creation, and are not necessary for the apps to run.

The DDL scripts can be run in any order; each script is for a separate schema.

If you need to clean the database before running the scripts, please run the following commands:
_Reminder that this will delete ALL records in the database. Use with extreme caution._

```sql
    DROP SCHEMA IF EXISTS auditlog CASCADE;
    DROP SCHEMA IF EXISTS cubelab_cases CASCADE;
    DROP SCHEMA IF EXISTS dataframes CASCADE;
```

### Authors:

Galen Nare

---

### License:

MIT

Feel free to use this code for whatever you want (credit appreciated but not required),
but if you claim you wrote it I will publicly shame you :)